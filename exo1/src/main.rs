fn main() {
    println!("Hello, world!");

    let _variable: i32 = 40;

    let mut i: i64 = 0;
    while i < 35 {
        println!("{}", i);
        i += 1;
    }

    let fred = Prof {
        name: String::from("Frédéric Alix"),
        age: 42,
        bon_prof: true,
        matiere: Matiere::Linux,
    };

    println!("Voilà la fiche de Fred: {:#?}", fred);

    let fred_converti = fred.changer_de_matiere(Matiere::Francais);

    println!("Voilà la fiche de Fred converti: {:#?}", fred_converti);

    // créer plus de profs
    let nathanael: Prof = Prof {
        name: String::from("Nathanael Elisabeth"),
        age: 22,
        bon_prof: true,
        matiere: Matiere::Maths,
    };

    println!("Voilà la fiche de Nathanael: {:#?}", nathanael);

    let nathanael_changementage = nathanael.changer_age(20);

    println!("Voilà la fiche de Nathanael avec un nouvel âge: {:#?}", nathanael_changementage);

    // mettre tous les profs dans une collection
    let mut allprofs= Vec::new();
    allprofs.push(fred_converti);
    allprofs.push(nathanael_changementage);


    // crée une boucle qui modifie et affiche tous les âges des profs à la suite
    for p in &allprofs {
        let plusageunan: i32 = p.age +1;
        println!("Son num est {:?} et son âge est de {:?} ans", p.name , plusageunan);
    }


}

#[derive(Debug)]
struct Prof {
    name: String,
    age: i32,
    bon_prof: bool,
    matiere: Matiere,
}

impl Prof {
    fn changer_de_matiere(mut self, matiere: Matiere) -> Prof {
        self.matiere = matiere;
        self
    }

    // todo: faire une fonction qui change l'âge
    fn changer_age(mut self, age: i32) -> Prof {
        self.age = age;
        self
    }
}

#[derive(Debug)]
enum Matiere {
    Maths,
    Francais,
    Anglais,
    Linux,
}